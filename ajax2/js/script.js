'use strict';
let page = 0;
$(function(){
    $("#get-data-button").click(function(){
        page = 1;
        $("#get-data-row").hide();
        $.get("ajax.php",function (data) {
            $("#data").html("");
            $("#navigation").show();
            //console.dir(data);
            render(data);
        },"json");
    });

    $("#nextBtn").click(function(){
        page++;
        if(page >3) page = 1;

        $.get("ajax.php",{"page": page},function (data) {
            $("#navigation").show();
            //console.dir(data);
            $("#data").html("");
            render(data);
        },"json");
    });

    $("#prevBtn").click(function(){
        page--;
        if(page < 1) page = 3;

        $.get("ajax.php",{"page": page},function (data) {
            $("#navigation").show();
            //console.dir(data);
            $("#data").html("");
            render(data);
        },"json");
    });

});

function render(data) {

    for(let i = 0; i < data.length; i++) {
        // формирование блоков
        let $wrapper = $("<div class='work wow fadeInDown animated' />");
        $("<img src='"+data[i].image+"'>").appendTo($wrapper);
        $("<h3>"+data[i].title+"</h3>").appendTo($wrapper);
        $("<p>"+data[i].text+"</p>").appendTo($wrapper);

        if(data[i].links !== undefined){
            let $workBottom = $("<div class='work-bottom' />");
            for(let $links = 0 ; $links < data[i].links.length; $links++){
                let $a = $("<a href='"+data[i].links[$links]+"' class='big-link-2 view-work'><i class=\"fa fa-search\"></i></a>");
                $a.appendTo($workBottom);
            }
            console.log($workBottom);
            $workBottom.appendTo($wrapper);
        }

        let $col = $("<div class='col-sm-4' />").append($wrapper);
        $("#data").append($col);
    }
}