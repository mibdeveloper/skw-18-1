<?php
$page = 1;
if(isset($_GET["page"])){
	$page = $_GET["page"];
}

$data = [
	new class {
		public $title = "Title1";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1493857671505-72967e2e2760?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1426290dbb34cadd419c620578d36dea&auto=format&fit=crop&w=1050&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title2";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1512798705819-f4be9c1ed36e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=4c61e3ba49f4717779d91a0f7a703004&auto=format&fit=crop&w=1500&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title3";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1517908954634-5c747a9ff10d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=70eb747469f47cd47b05bad6f5c7bcac&auto=format&fit=crop&w=750&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},

	new class {/*4*/
		public $title = "Title1";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1493689485253-f07fcbfc731b?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ebe5bb8654d3816c9d934ff455abffb2&auto=format&fit=crop&w=733&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title2";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1454165804606-c3d57bc86b40?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=37c91c8e3f63462e0739c676dfe8fee8&auto=format&fit=crop&w=750&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title3";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1464029902023-f42eba355bde?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5aaa65793c7b9e05accddb9c5e98b668&auto=format&fit=crop&w=750&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},

	new class {
		public $title = "Title1";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1493923216191-a9df49886a9f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=1b95afba333beb893dbd55241778aed5&auto=format&fit=crop&w=737&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title2";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1458783082186-f0f1e80c574d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e1eb42983e34f30727d883cf79cf2b71&auto=format&fit=crop&w=750&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
	new class {
		public $title = "Title3";
		public $text = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Atque eos ipsam labore vero. Recusandae, vitae!";
		public $image = "https://images.unsplash.com/photo-1512577107354-cc925a586320?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e08130e8cddc1460ae560709f25ef069&auto=format&fit=crop&w=750&q=80";
		public $links = ["https://facebook.com", "https://vk.com"];
	},
];

$arr = array_slice($data, ($page-1) * 3, 3);

echo json_encode($arr);