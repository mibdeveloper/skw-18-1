<?php
include "include/config.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <link rel="stylesheet" href="css/admin.css">

    <style>
        #menu { list-style-type: none; margin: 0; padding: 0; width: 60%; }
        #menu li { margin: 0 3px 3px 3px; padding: 0.4em; padding-left: 1.5em; font-size: 1.4em; height: 18px;
            border: 1px solid #aaa;}
        #menu li span { position: absolute; margin-left: -1.3em; }
    </style>

</head>
<body>

<!--ul>li(lorem2)*8-->

<ul id="menu">

    <?php
        $sql = 'SELECT * FROM menu ORDER BY sortPosition';
        foreach ($dbh->query($sql) as $row) {
           echo "<li>{$row['text']}</li>";
        }
    ?>


</ul>


<div id="dialog-confirm" title="Empty the recycle bin?">
    <label>New item:
        <input type="text" id="newItem">
    </label>
</div>

<a class="ui-button ui-widget ui-corner-all" id="addMenuItem" href="#">Add new menu item</a>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script>
    $(function () {

            $("#menu").sortable(
                {

                    start: function (event, ui) {
                        ui.item.startPos = ui.item.index();
                    }
                    ,
                    stop: function (event, ui) {
                        console.log("Start position: " + ui.item.startPos);
                        console.log("New position: " + ui.item.index());

                        //AJAX Request
                    }
                }

    )
    ;
    $("#sortable").disableSelection();


    // menu button

            $( "#addMenuItem" ).button();
            $( "#addMenuItem" ).click( function( event ) {
                event.preventDefault();
                $( "#dialog-confirm" ).dialog(
                    {
                        resizable: false,
                        height: "auto",
                        width: 400,
                        modal: true,
                        buttons: {
                            "Add": function() {

                                alert($("#newItem").val());
                                $( this ).dialog( "close" );
                            },
                            Cancel: function() {
                                $( this ).dialog( "close" );
                            }
                        }
                    }
                );
            } );



    });
</script>
</body>
</html>